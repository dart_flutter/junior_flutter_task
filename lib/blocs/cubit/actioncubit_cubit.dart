import 'package:bloc/bloc.dart';
import 'package:junior_test/model/RootResponse.dart';
import 'package:junior_test/model/actions/PromoItem.dart';
import 'package:junior_test/resources/api/mall_api_provider.dart';
import 'package:meta/meta.dart';

part 'actioncubit_state.dart';

class ActionCubit extends Cubit<ActioncubitState> {
  ActionCubit() : super(ActioncubitInitial());
  MallApiProvider mallApiProvider = MallApiProvider();
  RootResponse rootResponse;
  void fetchData() async {
    emit(ActionCubitLoading());
    final response = await mallApiProvider.fetchCards(0, 10);
    rootResponse = response;
    emit(ActionCubitSuccess(response.serverResponse.body.promo.list));
  }
}
