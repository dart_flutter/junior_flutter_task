part of 'actioncubit_cubit.dart';

@immutable
abstract class ActioncubitState {}

class ActioncubitInitial extends ActioncubitState {}

class ActionCubitLoading extends ActioncubitState {}

class ActionCubitFailed extends ActioncubitState {
  final String error;

  ActionCubitFailed(this.error);
}

class ActionCubitSuccess extends ActioncubitState {
  final List<PromoItem> promoItems;
  
  ActionCubitSuccess(this.promoItems,);
}
