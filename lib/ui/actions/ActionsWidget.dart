import 'dart:ui';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:junior_test/blocs/actions/ActionsItemQueryBloc.dart';
import 'package:junior_test/blocs/base/bloc_provider.dart';
import 'package:junior_test/blocs/cubit/actioncubit_cubit.dart';
import 'package:junior_test/resources/api/mall_api_provider.dart';
import 'package:junior_test/ui/actions/item/ActionsItemArguments.dart';

import 'item/ActionsItemWidget.dart';

class ActionsWidget extends StatefulWidget {
  ActionsWidget({Key key}) : super(key: key);

  @override
  _ActionsWidgetState createState() => _ActionsWidgetState();
}

class _ActionsWidgetState extends State<ActionsWidget> {
  ActionCubit actionCubit = ActionCubit();
  ActionsItemQueryBloc bloc = ActionsItemQueryBloc();
  @override
  void initState() {
    super.initState();
    actionCubit.fetchData();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Акции'),
        centerTitle: true,
      ),
      body: BlocConsumer<ActionCubit, ActioncubitState>(
        bloc: actionCubit,
        listener: (context, state) {
          if (state is ActionCubitSuccess) {}
        },
        builder: (context, state) {
          if (state is ActionCubitLoading) {
            return Center(
              child: CircularProgressIndicator(),
            );
          }
          if (state is ActionCubitSuccess && state.promoItems.isEmpty) {
            return Center(
              child: Text('No data'),
            );
          }
          if (state is ActionCubitSuccess && state.promoItems.isNotEmpty)
            return Padding(
              padding: const EdgeInsets.all(8.0),
              child: StaggeredGridView.countBuilder(
                crossAxisCount: 4,
                scrollDirection: Axis.vertical,
                physics: ScrollPhysics(),
                itemCount: state.promoItems.length,
                itemBuilder: (BuildContext context, int index) {
                  print(state.promoItems.length);
                  return Stack(
                    alignment: Alignment.center,
                    children: [
                      GestureDetector(
                        onTap: () {
                          bloc.addResultToController(
                            bloc.getController(),
                            actionCubit.rootResponse,
                          );
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                              builder: (_) => BaseBlocProvider(
                                bloc: bloc,
                                child: ActionsItemWidget(),
                              ),
                              settings: RouteSettings(
                                name: '/detail',
                                arguments: ActionsItemArguments(index),
                              ),
                            ),
                          );
                        },
                        child: CachedNetworkImage(
                          errorWidget: (context, url, error) => Center(
                            child: Text('No Data Found'),
                          ),
                          imageUrl: MallApiProvider.baseImageUrl +
                                  state.promoItems.elementAt(index)?.imgThumb ??
                              '',
                          imageBuilder: (context, imageProvider) =>
                              ColorFiltered(
                            colorFilter: ColorFilter.mode(
                                Colors.black.withOpacity(0.6),
                                BlendMode.darken),
                            child: Image(
                              image: imageProvider,
                              fit: BoxFit.fill,
                              colorBlendMode: BlendMode.darken,
                              filterQuality: FilterQuality.high,
                            ),
                          ),
                        ),
                      ),
                      Align(
                        alignment: FractionalOffset.center,
                        child: Container(
                          child: Text(
                            state.promoItems.elementAt(index)?.name,
                            overflow: TextOverflow.fade,
                            maxLines: 1,
                            softWrap: false,
                            style: TextStyle(
                                color: Colors.white,
                                fontSize: 18,
                                fontWeight: FontWeight.bold),
                          ),
                        ),
                      ),
                      Container(
                        child: Positioned(
                          bottom: 2,
                          right: 6,
                          child: Text(state.promoItems.elementAt(index).shop,
                              style:
                                  TextStyle(color: Colors.white, fontSize: 12)),
                        ),
                      )
                    ],
                    fit: StackFit.passthrough,
                  );
                },
                staggeredTileBuilder: (int index) => StaggeredTile.fit(2),
                mainAxisSpacing: 10.0,
                crossAxisSpacing: 10.0,
              ),
            );
          return Container(
            child: Text('No Data Received'),
          );
        },
      ),
    );
  }
}
