import 'package:flutter/material.dart';
import 'package:junior_test/ui/actions/ActionsWidget.dart';
import 'package:junior_test/ui/actions/item/ActionsItemWidget.dart';



void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      //home: ActionsWidget(),
      routes: {
        '/': (context) => ActionsWidget(),
        '/detail': (context) => ActionsItemWidget(),
            
      },
    );
  }
}
